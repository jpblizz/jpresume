import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  rescheduleDropPoint:any='';
  rescheduleBoardPoint:any='';
  resumeURL:any;
  linkedInURL : any;
  gitURL : any;
  fbURL : any;
  instaURL : any;
  stackOverflowURL : any;

  constructor() {
    
    this.linkedInURL = 'https://www.linkedin.com/in/jyoti-prakash-das-220706108/';
    this.gitURL = 'https://github.com/JyotiPrkash';
    this.fbURL = 'https://www.facebook.com/jyotiprakash.das.5074';
    this.instaURL = '';
    this.stackOverflowURL = 'https://stackoverflow.com/users/9449389/blizzard';

    this.resumeURL = 'https://drive.google.com/open?id=1wNZZI5Opkj8vC-ZqNJnqWshGYoPpT0Az';
   }
}
