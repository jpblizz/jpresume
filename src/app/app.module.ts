import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HashLocationStrategy, PathLocationStrategy, LocationStrategy, CommonModule, APP_BASE_HREF } from '@angular/common';
import { HttpModule, Headers, Http, RequestOptions, Response } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import * as $ from 'jquery';


import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { VersionComponent } from './version/version.component';

import { GlobalService } from './global.service';
// import { TypingAnimationDirective } from 'angular-typing-animation';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {Observable} from 'rxjs';  


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    VersionComponent,
    PageNotFoundComponent,
    // TypingAnimationDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [ { provide: LocationStrategy, useClass: HashLocationStrategy },
    GlobalService,],
  bootstrap: [AppComponent]
})
export class AppModule { }
