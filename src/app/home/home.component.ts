import { Component, OnInit, HostListener } from '@angular/core';
import { Http, Response } from '@angular/http';

import { map } from 'rxjs/operators';
import { GlobalService } from '../global.service';

import * as $ from 'jquery';
import Typed from 'typed.js';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  rolesArr  = ['Frontend Developer', 'Web Developer', 'Backend Developer', 'Fullstack Developer'];
  typeSpeed : number;
  typeCounter : number = 0;
  lastActive = "homeDiv";
  lastLiActive = "homeLi";
  lastActiveProfileTab = "aboutmeBtnDiv";
  lastActiveProfileDiv = "aboutmeDiv";
  private typewriter_text: string = "Frontend Developer";
  private typewriter_display: string = "";

  constructor( public global: GlobalService) { }

  ngOnInit() {
    // this.typeEffect();

    const options = {
      strings: ['Frontend Developer', 'Web Developer', 'Backend Developer', 'Fullstack Developer'],
      typeSpeed: 100,
      backSpeed: 50,
      showCursor: true,
      cursorChar: '_',
      loop: true
    };
    const typed = new Typed('.typed-element', options);
      // this.typingCallback(this);
  }

  openDiv(divID, liID){
    document.getElementById(this.lastLiActive+'I').style.color = "#CCCCCC";
    document.getElementById(this.lastLiActive+'P').style.color = "#CCCCCC";
    document.getElementById(this.lastLiActive+'P').style.fontStyle = "normal";
    document.getElementById(this.lastActive).style.display = "none";
    document.getElementById(divID).style.display = "block";
    this.lastActive = divID;
    document.getElementById(liID+'I').style.color = "#B0914F";
    document.getElementById(liID+'P').style.color = "#B0914F";
    document.getElementById(liID+'P').style.fontStyle = "italic";
    this.lastLiActive = liID;
  }

  openAboutDiv(divID, liID){
    document.getElementById(this.lastActiveProfileTab+'I').style.color = "#CCCCCC";
    document.getElementById(this.lastActiveProfileTab+'P').style.color = "#CCCCCC";
    document.getElementById(this.lastActiveProfileTab+'P').style.fontStyle = "normal";
    document.getElementById(this.lastActiveProfileTab+'I').style.borderTop = "0px solid #0C0C0C";
    document.getElementById(this.lastActiveProfileDiv).style.display = "none";
    document.getElementById(divID).style.display = "block";
    this.lastActiveProfileTab = divID;
    this.lastActiveProfileDiv = divID;
    document.getElementById(liID+'I').style.color = "#B0914F";
    document.getElementById(liID+'P').style.color = "#B0914F";
    document.getElementById(liID+'P').style.fontStyle = "italic";
    document.getElementById(liID+'I').style.borderTop = "1px solid #B0914F";
    this.lastActiveProfileTab = liID;
  }

  typeEffect(){
    var roleType = document.getElementById('roleIs');
    for(var i = 0; i<this.rolesArr.length; i++){
      for(var j = 0;j<this.rolesArr[i].length; j++){
        roleType.innerHTML += this.rolesArr[i][j];
      }
      if(i != 3){
        roleType.innerText = "";
      }
    }
  }

  onTypeComplete(){
    var roleDiv = document.getElementById('roleIs');
    roleDiv.innerHTML = '<span typingAnimation [typeSpeed]="30" [startDelay]="1000" (complete)="onTypeComplete()">Frontend Developer</span>';
  }

  downloadResume(){
    // Jp_fullStackCV
    window.open(this.global.resumeURL, '_blank');
    // window.location.href='assets/img/Jp_fullStackCV.pdf';
  }

  typingCallback(that) {
    let total_length = that.typewriter_text.length;
    let current_length = that.typewriter_display.length;
    if (current_length < total_length) {
      that.typewriter_display += that.typewriter_text[current_length];
    } else {
      that.typewriter_display = "";
    }
    setTimeout(that.typingCallback, 200, that);
  }

  openRefAcnt(whichAcnt){

    if(whichAcnt == "LN"){
      window.open(this.global.linkedInURL, '_blank');
    }else if(whichAcnt == "SO"){
      window.open(this.global.stackOverflowURL, '_blank');
    }else if(whichAcnt == "GH"){
      window.open(this.global.gitURL, '_blank');
    }else if(whichAcnt == "FB"){
      window.open(this.global.fbURL, '_blank');
    }else if(whichAcnt == "IG"){
      window.open(this.global.instaURL, '_blank');
    }

  }


}
